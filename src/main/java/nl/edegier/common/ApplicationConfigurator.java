package nl.edegier.common;

import java.util.logging.Logger;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import nl.edegier.common.qualifier.F1MDatasource;

/**
 * @author giererwi
 */
public class ApplicationConfigurator {
	
	@Produces @F1MDatasource
	@PersistenceContext(unitName="default")
	private EntityManager em;

	@Produces
	public Logger get(InjectionPoint ip) {
		Class<?> requestingClass = ip.getMember().getDeclaringClass();
		return Logger.getLogger(requestingClass.getName());
	}

}
