package nl.edegier.common;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import nl.edegier.model.Role;
import nl.edegier.model.User;
import nl.edegier.service.CrudService;

@Singleton
@Startup
public class ApplicationStartup {
	@Inject
	CrudService crudService;

	@PostConstruct
	private void createUsers() {
		User user = new User();
		user.setPassword("test");
		user.setEmail("test");
		Role role = new Role();
		role.setRoleName("user");
		user.addRole(role);
		crudService.persist(role);
		crudService.persist(user);
	}
}
