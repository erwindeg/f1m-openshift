package nl.edegier.common;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

public class RepoUtil {

	public static <T> T singleResult(TypedQuery<T> query) {
		try {
			return query.getSingleResult();

		} catch (NoResultException nre) {
			return null;
		}
	}
}
