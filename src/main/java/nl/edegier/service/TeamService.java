package nl.edegier.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import nl.edegier.model.Team;

@Stateless
public class TeamService {

	private static final int INITIAL_BUDGET = 100000000;

	@Inject
	CrudService crudService;

	public void saveTeam(Team team) {
		crudService.merge(team);
	}

	// public Team getTeam(String username){
	// return teamRepository.findByUsername(username);
	// }

	public Team createTeam(String username) {
		Team team = new Team(username);
		team.setBudget(INITIAL_BUDGET);
		return team;
	}

}
