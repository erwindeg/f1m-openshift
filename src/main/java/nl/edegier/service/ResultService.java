package nl.edegier.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import nl.edegier.common.qualifier.F1MDatasource;
import nl.edegier.integration.model.RaceResultItem;
import nl.edegier.integration.service.ErgastF1Service;
import nl.edegier.model.Driver;
import nl.edegier.model.DriverResult;
import nl.edegier.model.DriverResultWrapper;
import nl.edegier.model.Race;

@Stateless
public class ResultService {

	@Inject
	ErgastF1Service f1Service;

	@Inject
	CrudService crudService;

	@Inject
	@F1MDatasource
	EntityManager em;

	@Inject
	Logger logger;

	public void createRaceResults(Race race) {
		List<RaceResultItem> results = null;
		try {
			results = f1Service.getResults(race.getSeason().getSeasonYear(), race.getRound());
		} catch (Exception e) {
			logger.log(Level.WARNING, "ErgastF1Service.getResults failed");
		}

		if (results != null) {
			addRaceResult(race, results);
		} else {
			logger.log(Level.WARNING, "ErgastF1Service.getResults null");
		}
	}

	public void addRaceResult(Race race, List<RaceResultItem> results) {
		logger.info("adding results for race: " + race.getName());
		List<DriverResult> raceResult = new ArrayList<DriverResult>();

		for (RaceResultItem result : results) {
			logger.info("adding result " + result.getNumber() + "," + result.getPoints());
			DriverResult driverResult = new DriverResult();
			driverResult.setPosition(result.getPosition());
			driverResult.setStartNumber(result.getNumber());
			driverResult.setRace(race);
			driverResult.setPoints(result.getPoints());

			raceResult.add(driverResult);

		}

		race.setResults(raceResult);
		crudService.merge(race);
	}

	public List<DriverResultWrapper> getResultForRace(Long raceId) {
		Race race = crudService.find(Race.class, raceId);
		return getResultForRace(race);
	}

	public List<DriverResultWrapper> getResultForRace(Race race) {
		List<DriverResultWrapper> raceResults = new ArrayList<DriverResultWrapper>();
		List<DriverResult> driverResults = this.findResultsForRace(race);
		for (DriverResult result : driverResults) {
			DriverResultWrapper item = new DriverResultWrapper();
			Driver driver = crudService.findByProperty(Driver.class, "driverEntry.startNumber", result.getStartNumber());
			item.setDriver(driver);
			item.setDriverResult(result);
			raceResults.add(item);
		}
		return raceResults;
	}

	public List<DriverResult> findResultsForRace(Race race) {
		TypedQuery<DriverResult> query = em.createNamedQuery("findResultsForRace", DriverResult.class);
		query.setParameter("race", race);
		return query.getResultList();
	}

	public void setF1Service(ErgastF1Service f1Service) {
		this.f1Service = f1Service;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

}
