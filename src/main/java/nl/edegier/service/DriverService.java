package nl.edegier.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import nl.edegier.common.Parameters;
import nl.edegier.integration.service.ErgastF1Service;
import nl.edegier.model.Constructor;
import nl.edegier.model.Driver;
import nl.edegier.model.Engine;

@Stateless
public class DriverService {

	@Inject
	ErgastF1Service f1Service;

	@Inject
	CrudService crudService;

	@Inject
	Logger logger;

	public List<Driver> findAllDrivers() {
		return crudService.findAll(Driver.class);
	}

	public List<Constructor> findAllConstructors() {
		return crudService.findAll(Constructor.class);
	}

	public List<Engine> findAllEngines() {
		return crudService.findAll(Engine.class);
	}

	public void createDriversAndConstructors() {
		List<nl.edegier.integration.model.Constructor> constructors = null;
		try {
			constructors = f1Service.getConstructors(Parameters.CURRENT_SEASON.getIntValue());
		} catch (Exception e) {
			logger.log(Level.WARNING, "ErgastF1Service.getConstructors failed", e);
		}

		if (constructors != null) {
			createConstructors(constructors);
			createDrivers(constructors);
		} else {
			logger.log(Level.WARNING, "ErgastF1Service.getConstructors null");
		}
	}

	private void createDrivers(List<nl.edegier.integration.model.Constructor> constructors) {
		for (nl.edegier.integration.model.Constructor in : constructors) {

		}

	}

	private void createConstructors(List<nl.edegier.integration.model.Constructor> constructors) {
		// TODO Auto-generated method stub

	}

}
