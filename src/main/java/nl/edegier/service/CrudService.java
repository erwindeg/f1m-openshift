/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.edegier.service;

import static nl.edegier.common.RepoUtil.singleResult;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import nl.edegier.common.qualifier.F1MDatasource;

/**
 * @author giererwi
 * @param <T>
 *            The entity to use the CRUD on.
 */

@Stateless
public class CrudService {
	@Inject
	@F1MDatasource
	EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public <T> T persist(T entity) {
		this.entityManager.persist(entity);
		return entity;
	}

	public <T> T merge(T entity) {
		return this.entityManager.merge(entity);
	}

	public <T> void remove(T entity) {
		T attached = this.entityManager.merge(entity);
		this.entityManager.remove(attached);
	}

	public <T> T find(Class<T> entity, Long key) {
		return this.entityManager.find(entity, key);
	}

	public <T> List<T> findAll(Class<T> entity) {
		TypedQuery<T> query = entityManager.createQuery("SELECT e FROM " + entity.getSimpleName() + " e", entity);
		return query.getResultList();

	}

	public <T> T findByProperty(Class<T> entity, String property, Object value) {
		TypedQuery<T> query = entityManager.createQuery("SELECT e FROM " + entity.getSimpleName() + " e where e." + property + "=:value", entity);
		query.setParameter("value", value);
		return singleResult(query);
	}

	public <T> List<T> listByProperty(Class<T> entity, String property, Object value) {
		TypedQuery<T> query = entityManager.createQuery("SELECT e FROM " + entity.getSimpleName() + " e where e." + property + "=:value", entity);
		query.setParameter("value", value);
		return query.getResultList();
	}

}
