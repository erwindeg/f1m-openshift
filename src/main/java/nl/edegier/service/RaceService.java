package nl.edegier.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import nl.edegier.integration.model.RaceTable;
import nl.edegier.integration.service.ErgastF1Service;
import nl.edegier.model.Race;
import nl.edegier.model.Season;

@Stateless
public class RaceService {

	@Inject
	ErgastF1Service f1Service;

	@Inject
	CrudService crudService;

	@Inject
	Logger logger;

	public Race getRace(Long raceId) {
		return crudService.find(Race.class, raceId);
	}

	public Season createSeason(int year) {
		Season season = new Season();
		season.setSeasonYear(year);

		RaceTable raceTable = null;
		try {
			raceTable = f1Service.getSeason(year);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception getting season from f1 service", e);
		}
		if (raceTable != null) {
			for (nl.edegier.integration.model.Race race : raceTable.getRaces()) {
				nl.edegier.model.Race newRace = new nl.edegier.model.Race();
				newRace.setName(race.getRaceName());
				newRace.setSeason(season);
				newRace.setRound(race.getRound());
				newRace.setCircuit(race.getCircuit().getCircuitName());
				newRace.setEndDate(race.getDate());
				season.addRace(newRace);
			}
			crudService.persist(season);
			System.out.println("Created season " + year);
		} else {
			return null;
		}

		return season;
	}

	public List<Race> getRacesForSeason(int year) {
		return crudService.listByProperty(Race.class, "season", new Season(year));
	}
}
