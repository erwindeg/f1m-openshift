package nl.edegier.model;

public class DriverResultWrapper {
	
	private DriverResult driverResult;
	private Driver driver;
	
	
	public int getPosition(){
		return this.driverResult.getPosition();
	}
	
	public int getStartNumber(){
		return this.driverResult.getStartNumber();
	}
	
	public String getName(){
		return this.driver.getName();
	}
	public int getPoints(){
		return this.driverResult.getPoints();
	}
	public DriverResult getDriverResult() {
		return driverResult;
	}
	public void setDriverResult(DriverResult driverResult) {
		this.driverResult = driverResult;
	}
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver driver) {
		this.driver = driver;
	}

}
