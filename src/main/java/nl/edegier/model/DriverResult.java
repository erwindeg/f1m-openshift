package nl.edegier.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "findResultsForRace", query = "SELECT d FROM DriverResult d where d.race =  :race ORDER by d.position")
public class DriverResult implements Comparable<DriverResult>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2406180534077286770L;

	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne
	private Race race;

	private int startNumber;
	private int position;
	private int points;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Race getRace() {
		return race;
	}

	public void setRace(Race race) {
		this.race = race;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	@Override
	public int compareTo(DriverResult o) {
		// TODO Auto-generated method stub
		return Integer.valueOf(this.getPosition()).compareTo(o.getPosition());
	}

	public int getStartNumber() {
		return startNumber;
	}

	public void setStartNumber(int startNumber) {
		this.startNumber = startNumber;
	}

}
