package nl.edegier.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Driver implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -361562374912274186L;
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	@OneToOne(cascade=CascadeType.ALL)
	private DriverEntry driverEntry;
	
	private String externalKey;
	
	public Driver(){
		
	}

	public Driver(String name) {
		this.name=name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Driver other = (Driver) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DriverEntry getDriverEntry() {
		return driverEntry;
	}

	public void setDriverEntry(DriverEntry driverEntry) {
		this.driverEntry = driverEntry;
	}	
	
	public int getPrice(){
		return this.driverEntry.getPrice();
	}

	public String getExternalKey() {
		return externalKey;
	}

	public void setExternalKey(String externalKey) {
		this.externalKey = externalKey;
	}
}
