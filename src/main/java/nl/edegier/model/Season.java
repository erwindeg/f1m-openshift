package nl.edegier.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Season implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2406180534077286770L;
	
//	@Id
//	@GeneratedValue
//	private Long id;
	
	@Id
//	@Column(unique=true)
	private int seasonYear;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="season")
//	@OrderColumn(name="round")
	private List<Race> races = new ArrayList<Race>();

//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
	
	public Season(){
		
	}

	public Season(int year) {
		this.seasonYear=year;
	}

	public int getSeasonYear() {
		return seasonYear;
	}

	public void setSeasonYear(int seasonYear) {
		this.seasonYear = seasonYear;
	}

	public List<Race> getRaces() {
		return races;
	}

	public void setRaces(List<Race> races) {
		this.races = races;
	}

	public void addRace(Race race) {
		this.races.add(race);
	}
}
