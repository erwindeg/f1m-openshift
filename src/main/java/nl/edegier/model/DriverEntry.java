package nl.edegier.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class DriverEntry implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2406180534077286770L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	private Season season;
	@OneToOne
	private Driver driver;
	@ManyToOne
	private Constructor constructor;
	@ManyToOne
	private Engine engine;
	
	
	private int price;
	private int startNumber;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Season getSeason() {
		return season;
	}
	public void setSeason(Season season) {
		this.season = season;
	}
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	public Constructor getConstructor() {
		return constructor;
	}
	public void setConstructor(Constructor constructor) {
		this.constructor = constructor;
	}
	public Engine getEngine() {
		return engine;
	}
	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getStartNumber() {
		return startNumber;
	}
	public void setStartNumber(int startNumber) {
		this.startNumber = startNumber;
	}
	
	
	
	
}
