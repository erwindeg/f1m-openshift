package nl.edegier.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Race implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2406180534077286770L;

	@Id
	@GeneratedValue
	private Long id;
	
	
	@Column(name="round2")
	private Integer round;

	@ManyToOne
    @JoinColumn(name="season", nullable=false)
	private Season season;

	private String circuit;

	private String name;

	@Temporal(value = TemporalType.DATE)
	private Date endDate;

//	@ElementCollection
//    @MapKeyColumn(name="position")
//    @Column(name="driver")
//    @CollectionTable(name="position_driver", joinColumns=@JoinColumn(name="id"))
//	private Map<Integer,Driver> results;
	
	@OneToMany(cascade=CascadeType.ALL)
	@OrderColumn(name="position")
	private List<DriverResult> results;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Season getSeason() {
		return season;
	}

	public void setSeason(Season season) {
		this.season = season;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCircuit() {
		return circuit;
	}

	public void setCircuit(String circuit) {
		this.circuit = circuit;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DriverResult> getResults() {
		return results;
	}

	public void setResults(List<DriverResult> results) {
		this.results = results;
	}

	public Integer getRound() {
		return round;
	}

	public void setRound(Integer round) {
		this.round = round;
	}

}
