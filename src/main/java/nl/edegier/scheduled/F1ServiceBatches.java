package nl.edegier.scheduled;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;

import nl.edegier.common.Parameters;
import nl.edegier.model.Race;
import nl.edegier.model.Season;
import nl.edegier.service.CrudService;
import nl.edegier.service.RaceService;
import nl.edegier.service.ResultService;

@Stateless
public class F1ServiceBatches {

	@Inject
	Logger logger;

	@Inject
	CrudService crudService;

	@Inject
	ResultService resultService;

	@Inject
	RaceService raceService;

	@Schedule(persistent = false, hour = "*", minute = "*")
	public void retrieveResults() {
		logger.info("retrieveResults");

		Season season = crudService.findByProperty(Season.class, "seasonYear", Parameters.CURRENT_SEASON.getIntValue());
		if (season != null) {
			List<Race> races = season.getRaces();
			for (Race race : races) {
				// TODO: check if racedate is before today and if results are
				// empty
				if (race.getResults() == null || race.getResults().isEmpty()) {
					resultService.createRaceResults(race);
				} else {
					logger.info("Results for race " + race.getName() + " already exist");
				}

			}
		}
	}

	@Schedule(persistent = false, hour = "*", minute = "*")
	public void createSeason() {
		logger.info("createSeason");

		int year = Parameters.CURRENT_SEASON.getIntValue();
		Season season = crudService.findByProperty(Season.class, "seasonYear", Parameters.CURRENT_SEASON.getIntValue());
		if (season == null) {
			logger.info("Trying to create season " + year);
			raceService.createSeason(year);
		} else {
			logger.info("Season " + year + " already exists.");
		}
	}

	@Schedule(persistent = false, hour = "0/1")
	public void calculateScores() {
		// TODO: reschedule when no results availabe?
	}
}
