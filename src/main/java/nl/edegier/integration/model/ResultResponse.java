package nl.edegier.integration.model;

import javax.xml.bind.annotation.XmlRootElement;


import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultResponse {
	
	private ResultData mrData;
	
	public ResultResponse(){
		
	}

	@JsonProperty("MRData")
	public ResultData getMrData() {
		return mrData;
	}

	@JsonProperty("MRData")
	public void setMrData(ResultData mrData) {
		this.mrData = mrData;
	}	
}
