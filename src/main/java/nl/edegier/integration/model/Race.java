package nl.edegier.integration.model;

import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Race {
	private String raceName;
	private int round;
	private Date date;
	
	@JsonProperty("Circuit")
	private Circuit circuit;
	
	
	@JsonProperty("Results")
	private List<RaceResultItem> results; 
	
	
	public String getRaceName() {
		return raceName;
	}

	public void setRaceName(String raceName) {
		this.raceName = raceName;
	}

	public List<RaceResultItem> getResults() {
		return results;
	}

	public void setResults(List<RaceResultItem> results) {
		this.results = results;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Circuit getCircuit() {
		return circuit;
	}

	public void setCircuit(Circuit circuit) {
		this.circuit = circuit;
	}
}
