package nl.edegier.integration.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Constructor {
	private String constructorId;
	private String name;
	public String getConstructorId() {
		return constructorId;
	}
	public void setConstructorId(String constructorId) {
		this.constructorId = constructorId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
