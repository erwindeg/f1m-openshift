package nl.edegier.integration.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultData {
	private String series;
	@JsonProperty("RaceTable")
	private RaceTable raceTable;
	@JsonProperty("ConstructorTable")
	private ConstructorTable constructorTable;
	@JsonProperty("DriverTable")
	private DriverTable driverTable;
	public DriverTable getDriverTable() {
		return driverTable;
	}
	public void setDriverTable(DriverTable driverTable) {
		this.driverTable = driverTable;
	}
	public String getSeries() {
		return series;
	}
	public void setSeries(String series) {
		this.series = series;
	}
	public RaceTable getRaceTable() {
		return raceTable;
	}
	public void setRaceTable(RaceTable raceTable) {
		this.raceTable = raceTable;
	}
	public ConstructorTable getConstructorTable() {
		return constructorTable;
	}
	public void setConstructorTable(ConstructorTable constructorTable) {
		this.constructorTable = constructorTable;
	}
	
	
}
