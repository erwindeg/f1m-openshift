package nl.edegier.integration.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DriverTable {
	private String season;
	private String constructorId;
	
	@JsonProperty("Drivers")
	private List<Driver> drivers;
	public String getConstructorId() {
		return constructorId;
	}
	public void setConstructorId(String constructorId) {
		this.constructorId = constructorId;
	}
	public List<Driver> getDrivers() {
		return drivers;
	}
	public void setDrivers(List<Driver> drivers) {
		this.drivers = drivers;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
}
