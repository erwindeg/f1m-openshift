package nl.edegier.integration.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.ws.rs.core.MediaType;

import nl.edegier.common.Parameters;
import nl.edegier.integration.model.Constructor;
import nl.edegier.integration.model.Driver;
import nl.edegier.integration.model.RaceResultItem;
import nl.edegier.integration.model.RaceTable;
import nl.edegier.integration.model.ResultResponse;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

@Named
public class ErgastF1Service {

	String endpoint;

	@PostConstruct
	private void setEnpoint() {
		this.endpoint = Parameters.ERGAST_F1_ENDPOINT.getValue();
	}

	public List<Constructor> getConstructors(int year) throws Exception {
		ClientRequest request = new ClientRequest(endpoint + year + "/contructors.json");
		request.accept(MediaType.APPLICATION_JSON);

		ClientResponse<ResultResponse> clientResponse = request.get(ResultResponse.class);
		ResultResponse response = clientResponse.getEntity();

		return response.getMrData().getConstructorTable().getConstructors();
	}

	public List<Driver> getDrivers(String constructorId, int year) throws Exception {
		ClientRequest request = new ClientRequest(endpoint + year + "/contructors/" + constructorId + "/drivers.json");
		request.accept(MediaType.APPLICATION_JSON);

		ClientResponse<ResultResponse> clientResponse = request.get(ResultResponse.class);
		ResultResponse response = clientResponse.getEntity();
		return response.getMrData().getDriverTable().getDrivers();
	}

	public List<RaceResultItem> getResults(int year, int round) throws Exception {

		ClientRequest request = new ClientRequest(endpoint + year + "/" + round + "/results.json");
		request.accept(MediaType.APPLICATION_JSON);

		ClientResponse<ResultResponse> clientResponse = request.get(ResultResponse.class);
		ResultResponse response = clientResponse.getEntity();

		if (!validateResults(year, round, response)) return null;

		System.out.println(response.getMrData().getRaceTable().getSeason());
		System.out.println(response.getMrData().getRaceTable().getRound());
		System.out.println(response.getMrData().getRaceTable().getRaces().get(0).getRaceName());
		List<RaceResultItem> resultItems = response.getMrData().getRaceTable().getRaces().get(0).getResults();
		for (RaceResultItem item : resultItems) {
			System.out.println(item.getPosition() + ":" + item.getNumber() + ":" + item.getPoints());
		}

		return response.getMrData().getRaceTable().getRaces().get(0).getResults();
	}

	private boolean validateResults(int year, int round, ResultResponse response) {
		if (response == null) return false;
		if (response.getMrData() == null) return false;
		if (response.getMrData().getRaceTable() == null) return false;
		if (!("" + year).equals(response.getMrData().getRaceTable().getSeason())) return false;
		if (!("" + round).equals(response.getMrData().getRaceTable().getRound())) return false;
		if (response.getMrData().getRaceTable().getRaces() == null) return false;
		if (response.getMrData().getRaceTable().getRaces().size() == 0) return false;
		if (response.getMrData().getRaceTable().getRaces().get(0).getResults() == null) return false;
		if (response.getMrData().getRaceTable().getRaces().get(0).getResults().size() == 0) return false;
		return true;
	}

	public RaceTable getSeason(int year) throws Exception {
		// WebResource service = getResource(endpoint+year+".json");
		// GenericType<ResultResponse> genericType = new
		// GenericType<ResultResponse>() {};

		ClientRequest request = new ClientRequest(endpoint + year + ".json");
		System.out.println("Connection to: " + request.getUri());

		ClientResponse<ResultResponse> clientResponse = request.get(ResultResponse.class);
		ResultResponse response = clientResponse.getEntity();

		if (!validateSeason(year, response)) return null;
		return response.getMrData().getRaceTable();
	}

	private boolean validateSeason(int year, ResultResponse response) {
		if (response == null) return false;
		if (response.getMrData() == null) return false;
		if (response.getMrData().getRaceTable() == null) return false;
		if (!("" + year).equals(response.getMrData().getRaceTable().getSeason())) return false;
		if (response.getMrData().getRaceTable().getRaces() == null) return false;
		if (response.getMrData().getRaceTable().getRaces().size() == 0) return false;
		return true;
	}

}
