package nl.edegier.web;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.edegier.common.Parameters;
import nl.edegier.model.Race;
import nl.edegier.service.RaceService;

@Named
@RequestScoped
public class Calendar implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7366873739833016083L;
	@Inject
	RaceService raceService;

	public String getCurrentSeason() {
		return Parameters.CURRENT_SEASON.getValue();
	}

	public List<Race> getRaces() {
		List<Race> races = raceService
				.getRacesForSeason(Parameters.CURRENT_SEASON.getIntValue());
		return races;
	}

}
