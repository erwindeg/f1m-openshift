package nl.edegier.web;

import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

@Named
@SessionScoped
public class Authenticator {
	
//	public String login() throws LoginException {
//		//TODO: real (cm?) authentication, for now, just set the username on the session
//		return "team?faces-redirect=true";
//	}
	
	public String logout() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		request.getSession(false).invalidate();
		return "login?faces-redirect=true";
	}

	public String getUsername() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		return request.getUserPrincipal() != null ? request.getUserPrincipal().getName() : null;
//		return username;
	}

}
