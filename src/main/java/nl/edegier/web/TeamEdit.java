package nl.edegier.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import nl.edegier.model.Constructor;
import nl.edegier.model.Driver;
import nl.edegier.model.Engine;
import nl.edegier.model.Team;
import nl.edegier.service.CrudService;
import nl.edegier.service.DriverService;
import nl.edegier.service.TeamService;

@Named
@SessionScoped
public class TeamEdit implements Serializable {

	@Inject
	CrudService crudService;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private Authenticator authenticator;

	@Inject
	private TeamService teamService;

	@Inject
	DriverService driverService;

	private Team team;

	@PostConstruct
	void init() {
		if (team == null) team = crudService.findByProperty(Team.class, "username", authenticator.getUsername());
	}

	public List<Driver> getDrivers() {
		return driverService.findAllDrivers();
	}

	public List<Constructor> getConstructors() {
		return driverService.findAllConstructors();
	}

	public List<Engine> getEngines() {
		return driverService.findAllEngines();
	}

	public Team getTeam() {
		if (this.team == null) this.team = teamService.createTeam(authenticator.getUsername());
		return this.team;
	}

	public String save() {
		if (!valid()) return "editteam";
		if (team != null) teamService.saveTeam(team);
		return "team";
	}

	private boolean valid() {
		int total = 0;
		total += team.getDriver1() != null ? team.getDriver1().getPrice() : 0;
		total += team.getDriver2() != null ? team.getDriver2().getPrice() : 0;
		total += team.getConstructor() != null ? team.getConstructor().getPrice() : 0;
		total += team.getEngine() != null ? team.getEngine().getPrice() : 0;

		if (total > team.getBudget()) {
			FacesMessage message = new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary("Team is to expensive: " + total + " > " + team.getBudget());
			message.setDetail("Detail Team is to expensive");
			FacesContext.getCurrentInstance().addMessage("editTeam", message);
			return false;
		} else {
			return true;
		}
	}

}
