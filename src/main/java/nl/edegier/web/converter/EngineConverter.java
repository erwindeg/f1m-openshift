package nl.edegier.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import nl.edegier.model.Engine;
import nl.edegier.service.CrudService;

@Named
public class EngineConverter implements Converter{
	
	@Inject
	CrudService crudService;
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String name) {
		return crudService.findByProperty(Engine.class,"name",name);		
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object engine) {
		return ((Engine)engine).getName();
	}

}
