package nl.edegier.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import nl.edegier.model.Driver;
import nl.edegier.service.CrudService;

@Named
public class DriverConverter implements Converter {

	@Inject
	CrudService crudService;

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String name) {
		return crudService.findByProperty(Driver.class, "name", name);
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object driver) {
		return ((Driver) driver).getName();
	}

}
