package nl.edegier.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.edegier.common.Parameters;
import nl.edegier.model.DriverResultWrapper;
import nl.edegier.model.Race;
import nl.edegier.service.RaceService;
import nl.edegier.service.ResultService;

@Named
@RequestScoped
public class Results implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 566498592422546559L;

	@Inject
	RaceService raceService;

	@Inject
	ResultService resultService;

	// @ManagedProperty(value = "#{param.selectedRace}")
	private String selectedRace;

	public String getSelectedRaceName() {
		if (selectedRace == null) return "";
		return raceService.getRace(Long.valueOf(selectedRace)).getName();
	}

	public List<Race> getRaces() {
		List<Race> races = raceService.getRacesForSeason(Parameters.CURRENT_SEASON.getIntValue());
		return races;
	}

	public String show() {
		return "results";
	}

	public List<DriverResultWrapper> getRaceResults() {
		if (this.selectedRace != null) {
			return resultService.getResultForRace(Long.valueOf(selectedRace));
		}
		return new ArrayList<DriverResultWrapper>();

	}

	public String getSelectedRace() {
		// FacesContext facesContext = FacesContext.getCurrentInstance();
		//
		// this.selectedRace = (String) facesContext.getExternalContext()
		// .getRequestParameterMap().get("selectedRace");

		return this.selectedRace;
	}

	// public void valueChangeMethod(ValueChangeEvent event) throws IOException{
	// String page = (String) event.getNewValue(); // Must however be the exact
	// page URL. E.g. "contact.jsf".
	// FacesContext.getCurrentInstance().getExternalContext().redirect(page);
	// }

	public void setSelectedRace(String selectedRace) {
		this.selectedRace = selectedRace;
	}

}
