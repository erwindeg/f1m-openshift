package nl.edegier.integration.service;

import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.util.List;

import nl.edegier.integration.model.Race;
import nl.edegier.integration.model.RaceResultItem;
import nl.edegier.integration.model.RaceTable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class ErgastF1ServiceTest {

	HttpServer httpServer;

	ErgastF1Service f1Service;

	@Before
	public void setup() {
		f1Service = new ErgastF1Service();
		f1Service.endpoint = "http://localhost:8000/";
	}

	@Test
	public void testGetResults() throws Exception {
		List<RaceResultItem> result = f1Service.getResults(2012, 8);
		assertNotNull(result);

		RaceTable season = f1Service.getSeason(2012);
		assertNotNull(season);

		for (Race race : season.getRaces()) {
			System.out.println(race.getRound() + ", " + race.getRaceName()
					+ ", " + race.getDate() + " "
					+ race.getCircuit().getCircuitName());
		}
	}

	// @Test
	// public void pass() {
	// assertTrue(true);
	// }

	@Before
	public void startServer() {
		try {
			// create the HttpServer
			InetSocketAddress address = new InetSocketAddress(8000);
			httpServer = HttpServer.create(address, 0);

			final String resultsJson = readFileAsString("src/test/resources/results.json");
			// create and register our handler
			HttpHandler resultsHandler = new HttpHandler() {

				public void handle(HttpExchange exchange) throws IOException {

					byte[] response = resultsJson.getBytes();
					exchange.getResponseHeaders().add("Content-type",
							"application/json");
					exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK,
							response.length);
					exchange.getResponseBody().write(response);
					exchange.close();
				}
			};
			httpServer.createContext("/2012/8/results.json", resultsHandler);

			final String seasonJson = readFileAsString("src/test/resources/2012.json");
			// create and register our handler
			HttpHandler seasonHandler = new HttpHandler() {

				public void handle(HttpExchange exchange) throws IOException {

					byte[] response = seasonJson.getBytes();
					exchange.getResponseHeaders().add("Content-type",
							"application/json");
					exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK,
							response.length);
					exchange.getResponseBody().write(response);
					exchange.close();
				}
			};
			httpServer.createContext("/2012.json", seasonHandler);

			// start the server
			httpServer.start();
		} catch (Exception e) {
			e.printStackTrace();
			httpServer.stop(0);
		}
	}

	@After
	public void stopServer() {
		httpServer.stop(0);
	}

	private String readFileAsString(String filePath) throws java.io.IOException {
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}
		reader.close();
		return fileData.toString();
	}
}
