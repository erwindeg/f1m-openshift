package nl.edegier.data;

import static org.junit.Assert.assertNotNull;

import java.util.UUID;

import nl.edegier.model.Team;
import nl.edegier.service.CrudService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TeamRepositoryTest extends AbstractTest {

	CrudService service = new CrudService();

	@Before
	public void setup() {
		service.setEntityManager(getEntityManager());
		getTransaction().begin();
	}

	@Test
	public void testCRUD() {
		String username = UUID.randomUUID().toString();
		Team team = new Team(username);
		service.persist(team);
		assertNotNull(service.findAll(Team.class));
		assertNotNull(service.find(Team.class, team.getId()));
		assertNotNull(service.findByProperty(Team.class, "username", username));
	}

	@After
	public void teardown() {
		if (getTransaction().isActive()) {
			getTransaction().rollback();
		}
	}

}
