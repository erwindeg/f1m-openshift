package nl.edegier.data;

import static org.junit.Assert.assertTrue;
import nl.edegier.model.Race;
import nl.edegier.model.Season;
import nl.edegier.service.CrudService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RaceRepositoryTest extends AbstractTest {

	CrudService service = new CrudService();

	@Before
	public void init() {
		service.setEntityManager(getEntityManager());
		getTransaction().begin();
	}

	@Test
	public void testGetRaces() {
		Season season = new Season(2012);
		service.persist(season);
		Race race = new Race();
		race.setSeason(season);

		service.persist(race);
		assertTrue(service.listByProperty(Race.class, "season", season).size() > 0);
	}

	@After
	public void tearDown() {
		if (getTransaction().isActive()) {
			getTransaction().rollback();
		}
	}

}
