package nl.edegier.scheduled;

import java.util.logging.Logger;

import nl.edegier.common.Parameters;
import nl.edegier.data.AbstractTest;
import nl.edegier.integration.service.ErgastF1Service;
import nl.edegier.model.Race;
import nl.edegier.model.Season;
import nl.edegier.service.CrudService;
import nl.edegier.service.ResultService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class F1ServiceBatchesIntegrationTest extends AbstractTest {

	ResultService resultService;
	CrudService crudService;
	F1ServiceBatches f1ServiceBatches;

	@Before
	public void setup() {
		f1ServiceBatches = new F1ServiceBatches();
		f1ServiceBatches.logger = Logger.getAnonymousLogger();

		crudService = new CrudService();
		crudService.setEntityManager(getEntityManager());

		resultService = new ResultService();
		resultService.setLogger(Logger.getAnonymousLogger());
		ErgastF1Service ergastF1Service = new ErgastF1Service();

		resultService.setF1Service(ergastF1Service);

		f1ServiceBatches.resultService = resultService;
		getTransaction().begin();
	}

	@Test
	public void testGetResults() {
		int year = Parameters.CURRENT_SEASON.getIntValue();
		Season season = new Season();
		season.setSeasonYear(year);
		Race race = new Race();
		race.setRound(8);
		race.setSeason(season);
		season.addRace(race);
		crudService.persist(season);

		// TODO: check if rest client works and fix this test
		f1ServiceBatches.retrieveResults();

		race = crudService.find(Race.class, race.getId());
		// assertNotNull(race.getResults());
	}

	@After
	public void teardown() {
		if (getTransaction() != null && getTransaction().isActive()) {
			getTransaction().rollback();
		}
	}
}
