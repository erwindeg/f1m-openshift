package nl.edegier.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import nl.edegier.data.AbstractTest;
import nl.edegier.integration.model.RaceResultItem;
import nl.edegier.integration.service.ErgastF1Service;
import nl.edegier.model.Driver;
import nl.edegier.model.DriverEntry;
import nl.edegier.model.DriverResult;
import nl.edegier.model.DriverResultWrapper;
import nl.edegier.model.Race;
import nl.edegier.model.Season;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class ResultServiceTest extends AbstractTest {

	CrudService crudService;
	ResultService resultService;

	@Before
	public void setup() {
		resultService = new ResultService();
		resultService.logger = Logger.getAnonymousLogger();
		resultService.em = getEntityManager();

		crudService = new CrudService();
		crudService.setEntityManager(getEntityManager());
		resultService.crudService = crudService;

		resultService.f1Service = Mockito.mock(ErgastF1Service.class);
		getTransaction().begin();
	}

	@Test
	public void testAddResults() {
		Driver driver = new Driver("driver1");
		DriverEntry entry = new DriverEntry();
		entry.setStartNumber(1);
		entry.setDriver(driver);
		driver.setDriverEntry(entry);
		crudService.persist(driver);

		Driver driver2 = new Driver("driver2");
		DriverEntry entry2 = new DriverEntry();
		entry2.setStartNumber(5);
		entry2.setDriver(driver2);
		driver2.setDriverEntry(entry2);
		crudService.persist(driver2);

		List<RaceResultItem> results = new ArrayList<RaceResultItem>();
		RaceResultItem r1 = new RaceResultItem();
		r1.setNumber(1);
		r1.setPosition(1);
		r1.setPoints(25);
		results.add(r1);

		RaceResultItem r2 = new RaceResultItem();
		r2.setNumber(5);
		r2.setPoints(18);
		r2.setPosition(2);
		results.add(r2);

		Race race = new Race();
		Season season = new Season(2012);
		crudService.persist(season);
		race.setSeason(season);
		crudService.persist(race);
		resultService.addRaceResult(race, results);

		assertNotNull(race.getResults());
		assertEquals(driver.getDriverEntry().getStartNumber(), race.getResults().get(0).getStartNumber());

		assertTrue(race.getResults().get(0).getPosition() < race.getResults().get(1).getPosition());
	}

	@Test
	public void testGetRaceResults() {
		Driver driver = new Driver();
		DriverEntry entry = new DriverEntry();
		driver.setName("Driver1");
		driver.setDriverEntry(entry);
		crudService.persist(driver);
		entry.setDriver(driver);
		entry.setStartNumber(1);

		Race race = new Race();
		Season season = new Season(2012);
		crudService.persist(season);
		race.setSeason(season);
		crudService.persist(race);

		DriverResult result = new DriverResult();
		result.setStartNumber(1);
		result.setPosition(1);
		result.setRace(race);
		crudService.persist(result);

		List<DriverResultWrapper> results = resultService.getResultForRace(race);

		for (DriverResultWrapper resultEntry : results) {
			System.out.println(resultEntry.getPosition());
			System.out.println(resultEntry.getName());
		}

		assertTrue(results.size() == 1);
	}

	@After
	public void teardown() {
		if (getTransaction() != null && getTransaction().isActive()) {
			getTransaction().rollback();
		}
	}
}
