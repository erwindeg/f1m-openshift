package nl.edegier.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import nl.edegier.data.AbstractTest;
import nl.edegier.integration.model.RaceTable;
import nl.edegier.integration.service.ErgastF1Service;
import nl.edegier.model.Race;
import nl.edegier.model.Season;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class RaceServiceTest extends AbstractTest {

	RaceService raceService;
	ErgastF1Service f1Service;
	CrudService crudService;

	@Before
	public void setup() throws Exception {
		crudService = new CrudService();
		crudService.entityManager = getEntityManager();
		raceService = new RaceService();

		raceService.crudService = crudService;
		f1Service = Mockito.mock(ErgastF1Service.class);
		RaceTable raceTable = new RaceTable();
		raceTable.setRaces(new ArrayList<nl.edegier.integration.model.Race>());
		Mockito.when(f1Service.getSeason(Mockito.anyInt())).thenReturn(raceTable);
		raceService.f1Service = f1Service;

		getTransaction().begin();
	}

	@Test
	public void testCreateSeason() {
		int randomYear = Long.valueOf(System.currentTimeMillis()).intValue();
		Season season = raceService.createSeason(randomYear);
		assertNotNull(crudService.findByProperty(Season.class, "seasonYear", randomYear));

		Race race = new Race();
		race.setName("GP Spain");
		race.setCircuit("Cataluya");
		race.setSeason(season);

		season.addRace(race);
		crudService.merge(season);

		assertEquals(season, race.getSeason());
	}

	@After
	public void teardown() {
		if (getTransaction() != null && getTransaction().isActive()) {
			getTransaction().rollback();
		}
	}
}
